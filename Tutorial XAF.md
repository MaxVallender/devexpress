Hinweis: Die Verwendung deutscher Bezeichnungen ist bewusst gewählt, da es einem ermöglicht entsprechende Stellen im Code leichter wiederzufinden. ;-)

---

# Basics

	.Module
	.Module.Win
	.Win

Die **.Win** als Startprojekt festlegen.
Es wirft auch immer wieder Fehler auf, wenn insbesondere die .Module als Startprojekt festgelegt ist.

#		D E S I G N E R

ACHTUNG: Möglichst nach jeder Änderung speichern, damit Auswirkungen auch in den zugehörigen Klassen umgesetzt werden können.

## Klassendiagramm

Unter **BusinessObects**, mit dem rechter Mausklick **Add DevExpress Item** aufrufen. **XPO Business Object...** auswählen.

##		Properties

Links befindet sich eine Toolbox. Aus diesre **Persistent Object** und **Persistent Property** entsprechend entnehmen.

Beim **Persistent Object**:

	Custom Attributes
		DevExpress.Persistent.Base.DefaultClassOptions <- Um es sichtbar zu machen. Alternativ über den Model-Designer (später).
	Name
		KlassenName


Bei **Persistent Property**:

	Column Type
		Typ auswählen
	Name
		PropertyName


Bei Verwendung von Datum:

	Column Type
		DateTime
	Db Type
		datetime2
	Value Converter
		UtcDateTimeConverter


## Relationen

Hinweis, beim Verbinden:

### Definition in UML

Aggregation:

	       Baum ◇----- Blatt

Ein Baum und ein Blatt können voneinander unabhängig vorhanden sein.

Komposition (auch *Composite Aggregation*):

	    Gebäude ◆----- Raum

Ein Gebäude und ein Raum können dies nicht. 
(Ein Gebäude besteht aus Räumen. Und ein Raum entsteht in einem Gebäude. Daher macht es Sinn, dieses direkt bei der Initialisierung zu verlangt.)

### Verwendung in DevExpress

Im Gegensatz zu UML wird hier die Komposition durch einen Pfeil dargestellt.

	    ◆-----  ist hier  ----->
		
(In der Toolbox wird es allerdings als Symbol ---- dargestellt.)

Die Many-to-Many-Relation ist in der **Toolbox** eine offene Raute der Aggregation (◇). Beim Verbinden allerdings die Pfeile der Komposition, nach beiden Seiten.

	    ◇  wird bei Anwendung zu  <---->

Kommen wir nun dazu diese zur Verbindung zu nutzen.
Wollen wir eine **One** to **Many** Beziehung darstellen, so muss auf der **Many**-Seite im Objekt, ein Property vom Typ des anderen Objektes erstellt werden.
(Auf der **One**-Seite wird dagegen im nächsten Schritt automatisch eine **Collection** erzeugt.)

In der **Toolbox** ist zB die **Aggregated One-To-Many Relationship**, etwas so dargestellt -----◇. Man wendet diese an, in dem man diese dann von links (**One**) nach rechts (**ZeroMany**) anwendet. Dahin wo man zuerst klickt ist die **One**-Seite und die andere Seite **ZeroMany**. 

Jedoch ändert sich die Richtung, gegenüber der im Symbol angezeigten Darstellungsrichtung, in ◇----- um. Dies stimmt nicht mit der im Symbol angezeigten Richtung überein. (Allerdings wieder mit der UML-Konvention.)

FRAGEZEICHEN Während normal die Komposition die Sonderform ist. : ist die Aggregation eher die Sonderform der Assoziation.

##		Klassen - Vererbung

In der Toolbox als ----ᐅ dargestellt.

Als Standard wird Designer von der Klasse **XPObject** geerbt.

	        PersitentBase           | 
	             |                  | 
	        XPBaseObject            | 
	          |      |              | 
	XPLiteObject   XPCustomObject   | XPCustomObject: + Deferred Deletion
	                 |              | 
	               XPObject         | + OID (+ Deferred Deletion)
	
	                 mitte/rechts: + Optimistic Locking

Mittels **OID** wird ein **Object IDentifier** vergeben.
**Locking** regelt die Schreibzugriffe auf die Datenbank

> These are: pessimistic concurrency, optimistic concurrency and "Last in wins". eXpress Persistent Objects supports the last two.

Bei **Deferred Deletion** wird ein Löschen der Objekte unterbunden. Statt dessen werden sie nur als gelöscht markiert.


##		Persistent

Text


## Proberties/Attibute

Wenn ein Propertie zusätzlich Attibute erhalten soll, wie *[System.Runtime.Serialization.DataMember]*. So kann man dies über **Custom Attributes** hinzufügen.
Es muss dann in der Form *Newtonsoft.Json.JsonProperty, System.Runtime.Serialization.DataContract* sein. (Die entsprechenden Pakete müssen natürlich über NuGet hinzugefügt werden.)


## .Designer

	Typ fProperty;			// die eigentliche Variable
	public Typ Property
	{
		get { return fProperty; }
		set { SetPropertyValue<Typ>("Property", ref fProperty, value); }
	}

Folgendes Beispiel. In *Class A* wird auf *Class B* referenziert. Diese wiederrum enthält *As* als Liste. Durch die Angabe von *AReferencesB* wird diese auf beiden Seiten immer synchron gehalten.

	public partial Class A : XPCustomObject {
		B fB;
		[Association(@"AReferencesB")]
		public B B
		{
			get { return fB; }
			set { SetPropertyValue<B>(nameof(B), ref fB, value); }
		}
	}

	public partial Class B : XPCustomObject {
		[Association(@"AReferencesB")]
		public XPCollection<A> As { get { return GetCollection<A>(nameof(As)); } }
	}

---

#		M O D E L E R

Innerhalb der Projektmappe findet man eine Datei **Model*.xafml**. In dieser lässt sich die Darstellung des Programms modellieren. Gerade bei dieser darauf achten regelmäßig zu speichern. Teilweise öffnet sie sich auch nicht mehr.

Möglichst immer die innerste öffnen! Meist jene von **.Module**.
Wenn Änderungen gemacht wurden, speichern. Möglichst alles schließen. Dann im **Projektmappe-Explorer** **Update Model on all Projects** ausführen!

Unter **Application** befinden sich folgende Punkte:

 * **ActionDesign** - Hier befinden sich ausführbare **Actions**, insbesondere die  **Controllers** sind gelistet.
 * **BOModel** - Unter dem Projektname findet man die eigens angelegten BusinessObjekte (Klassen) des Moduls.
 * **CreatableItems** - 
 * **ImageSources** - Listet eigene Icons usw.
 * **Localization** - Länderspezifische Anpassungen FRAGEZEICHEN
 * **NavigationItems** - Die links erscheinenden Items lassen sich hier editieren.
 * **Options** - 
 * **ViewItems** - 
 * **View** - Unter dem Projektnamen findet man auch hier die eigens angelegten BusinessObjekte (Klassen) und deren Darstellung innerhalb der View.
   * zB **.Module.BusinessObjects.Database** -> _ListView -> Columns -> X -> LookupProperty -> auf eine Eigenschaft des Objekts verweißen, die jetzt anstelle des Objekts angezeigt werden soll.

Änderung lassen sich reseten. Auch für ganze Zweige.

---

#		S E R V E R - V E R B I N D U N G

Es sollte neben dem **Microsoft SQL Server** auch das **Microsoft SQL Server Managment Studio** installiert sein.
Über den **SQL Server Configuration Manager**, lässt sich nach möglichen Fehlerursachen gucken. (Bei mir war nach einer Neuinstallation unter **SQL Server-Netzwerkkonfiguration**, **Protokolle für 'MSSQLSERVER', **TCP/IP** mal nicht auf **Aktiviert**.)
Durch das Managment Studio bekommt man insbesondere Zugriff auf die vom Server zur Verfügung gestellten Datenbanken.

Im Projekt muss der ConnectionString eingerichtet werden. Für diesen ergibt sich nachfolgendes Bild.

 * in **.Module** die Datei **app.config**:

	<add name="IF-PC51Test04062021" connectionString="XpoProvider=MSSqlServer;data source=IF-PC51;integrated security=SSPI;initial catalog=Test04062021" /> 

 * in **.Win** die Datei **App.config**:

	<add name="EasyTestConnectionString" connectionString="Integrated Security=SSPI;Pooling=false;Data Source=(localdb)\mssqllocaldb;Initial Catalog=ProjektnameEasyTest"/>
	<add name="ConnectionString" connectionString="Integrated Security=SSPI;Pooling=false;Data Source=(localdb)\mssqllocaldb;Initial Catalog=Projektname" />

Mit "> sqlcmd -L" erhält man NICHT die Liste der verfügbaren SQL-Server!
Warum auch immer? IF-PC51 ist nicht gelistet. Obwohl er ja nach vorherigen Angaben, als Server fungieren müsste. Sonst würde der Code ja nicht laufen können.

Soweit mir bekannt ist gehört dazu:

 * **name**
 * **ConnectionString**
 * **providerName** Optimal?

Der **ConnectionString** kann folgenden Eigenschaften fassen:

 * **Data Source** = Server. Der Server zu dem die Verbindung besteht. (Hat weniger mit einer einseitigen Quelle zu tun.) **(LocalDB)\MsSqlLocalDB**
 * **Initial Catalog** = Database. Die DB auf dem Server. (Es hat nichts mit einem Katalog zu tun, der zur Initialisierung gentzt werden soll. Eher wurde schon als solche initialisiert oder soll als diese initialisiert werden.)
 * **Integrated Security** ? Default? Kann **true**, **SSPI** sein
 * **Pooling** ? Default? Kann **true/false** sein
 * **Persist Security Info** ? Default? Kann **true** sein
 * **Read Only** ? Default? Kann **true/false** sein

Im Controller besteht keine unmittelbare Verbindung zur Datenbank. Diese kann zB durch folgenden Code hergestellt werden.

ToDo: Anderer Weg - vielleicht: var bla = (XPObjectSpace)Application.CreateObjectSpace();

	string actConnectionString = XpoDefault.ConnectionString; // Only for information.

	string connectionString = @"Integrated Security=SSPI;Pooling=false;Data Source=IF-PC51;Initial Catalog=Solution3";
	IDataLayer dataLayer = XpoDefault.GetDataLayer(connectionString, DevExpress.Xpo.DB.AutoCreateOption.DatabaseAndSchema);

	//XpoDefault.DataLayer = dataLayer;
	//using (UnitOfWork uow = new UnitOfWork()) {
	// OR
	using (UnitOfWork uow = new UnitOfWork(dataLayer)) {
		GeneralXpObject1 newObj = new GeneralXpObject1(uow);
		newObj.Field1 = "NEW";

		uow.CommitChanges();
	}
	
	// Angenommen - hier - ständen jetzt noch Änderungen (an bestehenden Objekten. Dann führt nächste Zeile diese Änderungen durch.
	//View.ObjectSpace.CommitChanges();

	View.ObjectSpace.Refresh(); // Nicht notwendig. Anzeige wird aktuallisiert.

Wenn Objekte gelöscht werden sollen

	// einzeln
	View.ObjectSpace.Delete(objekt);
	
	// viele
	IList<Objekttyp> objekts = View.ObjectSpace.GetObjects<Objekttyp>();
	View.ObjectSpace.Delete(objekts);

	View.ObjectSpace.CommitChanges();

---

Worum geht es hier??

	ExchangeInboxList exlist = new ExchangeInboxList();
	using (IObjectSpace lobj = (XPObjectSpace)Application.CreateObjectSpace())
		{
			exlist.loaditems(lobj)




OR

	private void Application_ListViewCreating(Object sender, ListViewCreatingEventArgs e)
	{
		if ((e.CollectionSource.ObjectTypeInfo.Type == typeof(ExchangeInboxItem)) && (e.CollectionSource.ObjectSpace is NonPersistentObjectSpace))
		{
				((NonPersistentObjectSpace)e.CollectionSource.ObjectSpace).ObjectsGetting += ObjectSpace_ObjectsGetting;
		}
	}

### Datenbank Schema aktualisieren

Es gibt Probleme mit neu erstellten Elementen.
Wenn folgende Meldung erscheint sind meistens Änderungen am Datenbankmodell durchgeführt worden. Die neuen Tabellen sind auch nicht im SQL-Server angelegt worden.

	Das Schema muss aktualisiert werden. Bitte den Systemadministrator kontaktieren. Sql Text: Ungültiger Objektname "dbo.DeinObjektName".
	
	Ungültiger Objektname "dbo.DeinObjektName".
	...

Es hilft den Versions-Counter hochzuzählen.
Diesen findet man (meist) unter **.Win**, **GlobalAssemblyInfo.cs**.
Dort diese beiden Zeilen editieren. Indem von hinten, die Version hochgezählt wird.

	[assembly: AssemblyVersion("1.2.3.2004")]        ->    [assembly: AssemblyVersion("1.2.3.2005")]
	[assembly: AssemblyFileVersion("1.2.3.2004")]    ->    [assembly: AssemblyFileVersion("1.2.3.2005")]

---

#		C O N T R O L L E R

Will man eine zusätzliche Schaltfläche in einem der Leisten/Menüs angezeigt bekommen, so erstellt man einen Controller. Dieser kann über eine Action eine Interaktion bewirken.

Dazu klickt man rechts im **Projektmappen-Explorer** unter dem Punkt **Controllers**, auf **Add DevExpress Item**. Dort **New Item...** und wählt den **View Contoller** aus.
Bennenen diesen als **meinController.cs**. (Er darf **NICHT** ViewController.cs heißen! Dadurch kollidiert er mit der ebenfalls so benannten Klasse.)
Zunächst erscheint eine leere Fläche.
(Meist) links, gibt es eine Leiste **Toolbox**. Aus dieser verwenden wir hier die **SimpleAction**. Einfach auf die Oberfläche ziehen. Dort befindet sich jetzt ein runder Button mit **simpleAction1**.
Bennene diesen um, in **meineAction**. Dazu rechts unten bei den Eigenschaften:

 * **(Name)** - dies ist der Name der Action, wie wir sie als Programmierer verwenden
 * **Id** - dies ist der Bezeichner mit der sie im Fenster erscheinen wird

zusätzlich:

 * **TargetObjectType** - das BusinessObject auswählen, bei dem die Schaltfläche erscheinen soll

Neben den Eigenschaften gibt es noch eine Liste mit den **Ereignisse**. Dort unter:

 * **Execute** - wird, die auszuführende Aktion festgelegt. (Mehrfach ins freie Feld klicken.)

Man landet an der entsprechenden Stelle in der Code-Ansicht, der **meinController.cs**.
Es sollte die **meineAction_Execute(...)** angelegt worden sein.
Hier kann man jetzt seinen Code hinterlegen. (zB **Console.WriteLine("Irgendetwas");**).
Speichern nicht vergessen!

Angenommen es gäbe ein Objekt Objekttyp, mit der einer stringVariable vom Typ String.
Dann würde folgender Code den Wert der Variable ändern.

	var obj = e.CurrentObject as Objekttyp;
	if (obj is OBJEKTTYP o) {
		o.stringVariable = "Der Controller kann ändern!";
	}

Beim Aufruf des Programms sollte jetzt an der entsprechenden Stelle eine Schaltfläche **meineAction** sein.
Klickt man diese sollte in der Ausgabe **Irgendetwas** erscheinen.

Weiter Optionen sind:

 * **ActionMeaning** - 
 * **Caption** - 
 * **Category** - 
 * **ItemType** - 
 * **TargetViewId** - 
 * **TargetViewNesting** - 
 * **TargetViewType** - 
 * **ToolTip** - Hinweis, der beim Überfahren mit der Maus erscheint
 * **TypeOfView** - 

---

#		E X P R E S S I O N S

https://docs.devexpress.com/XtraReports/120104/detailed-guide-to-devexpress-reporting/use-expressions/expression-constants-operators-and-functions

...

Bei einigen Expressions will man über andere Objekte mithilfe der Collections gehen. Zu beachten ist, dass diese auch leer sein können. Daher sollte gegen **null** gecheckt werden. Es gibt dafür die Funktion **IsNull**. Unter den Funktionen wird lediglich jene angezeigt, die **true** zurückgibt, wenn der Parameter **null** ist.
Dies hilft uns jedoch nur geringfügig weiter, wenn ein spezieller Rückgabetyp erwartet wird.
Jedoch gibt es weiterhin eine mit zwei Parametern, bei denen der erste auf **null** gecheckt wird und an dessen Stelle der zweite Parameter zurückgegeben wird.
Gerade beim Summieren über Objekten empfiehlt es sich als zweiten Parameter etwas wie **ToDecimal(0)** an zweiter Stelle anzugeben.
Will man zB über eine Collection summieren, konkret über den Wert der **Zahl**, so sieht es etwas folgendermaßen aus:

	**IsNull( [Collections][].Sum([Zahl]), ToDecimal(0) )**

Allerdings habe ich unter DevExpress öfters schlechte Erfahrungen damit gemacht. Es kam zu Fehlern und mein Ausdruck war zu

	**[Collections][].Sum([Zahl]) IsNull**

umgeschrieben worden. Es ergibt - gerade bei komplexe Ausdrücke - Sinn diese zwischenzuspeichern!!


## Formatierung

Es lassen sich Formatvorlagen nutzen, wie **{0:P1}**

# Authentifizierung

Kann beim erstellen des Projektes vorgegeben werden.
Auch die von Microsoft verwendete Authentifizierung kann verwendet werden.

Andere:
Neu angelegt haben User keine Chance sich einzuloggen.
ToDo: Selbst wenn man den Nutzer den Administrators zufügt!?

In der **.Module\DatabaseUpdate\Updater.cs** können mögliche Default-Nutzer und deren Passworter stehen.

---

#		R E P O R T

---

#		P R O J E K T E ??

XAF Solution Wizard: add Web - Win + Web
   
Add DevExpress Item -> New Project...
XAF Solutions
XAF Solution Wizard
Win_Web_SimpleContact
ASP.NET Application Project
eXpress Persistent Object

---

## Blazor

	.Blazor.Server	<- übliches Schema wäre ohne .Server
	.Module
	.Module.Blazor

#		n i c h t _ z u g e o r d n e t

Fehlersuche
Wenn Properties/Attibute nicht sichtbar sind. Obwohl sie eigentlich sichtbar sein sollten. Es keine vernünftige Erklärung dafür zu geben scheint. Einfach mal etwas wie ein X hintersetzen. Bei Begriffen wie **Name** hatte ich öfters Probleme. Mit X dahinter erschien es dann.



Integrated Security=SSPI;Pooling=false;Data Source=IF-PC51;Initial Catalog=iXIS16062021;Persist Security Info=true

ObjectSpace als universell zugreifbares Objekt. Braucht dann nicht an Methoden, innerhalb Klasse, weitergegeben werden.

#		P O W E R S H E L L


	> Install-Module XpandPwsh -AllowClobber


	> Clear-NugetCache
	Clear-NugetCache : The 'Clear-NugetCache' command was found in the module 'XpandPwsh', but the module could not be loaded. For more information, run 'Import-Module XpandPwsh'.
	...

	> Import-Module XpandPwsh
	Import-Module : File C:\Program Files\PowerShell\Modules\XpandPwsh\1.211.0.5\XpandPwsh.psm1 cannot be loaded. The file C:\Program Files\PowerShell\Modules\XpandPwsh\1.211.0.5\XpandPwsh.psm1 is not digitally signed. You cannot run this script on the current system. For more information about running scripts and setting execution policy, see about_Execution_Policies at https://go.microsoft.com/fwlink/?LinkID=135170.
	...

	> Get-ExecutionPolicy -List
	
        	Scope ExecutionPolicy
	        ----- ---------------
	MachinePolicy       Undefined		<- "Group Policy for all users" 		(difference to 5?)
	   UserPolicy       Undefined		<- "Group Policy for the current user"		(difference to 4?)
	      Process       Undefined		<- "current PowerShell session"
	  CurrentUser       Undefined		<- "current user"				(difference to 2?)
	 LocalMachine       AllSigned		<- "all users"					(difference to 1?)


	> Set-ExecutionPolicy -ExecutionPolicy <PolicyName> -Scope <scope>

	> Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope LocalMachine


Die ersten beiden Group Policies können nicht einfach umgeändert werden. 
Falls mal relevant:
Über Windows Suche nach "Group Policy Management Editor" OR "Gruppenrichtlinien bearbeiten" suchen.

	Richtlinien für Lokaler Computer
		Computerkonfiguration
			Administrative Vorlagen
				Windows-Komponenten
					Windows Powershell
						Skriptausführung aktivieren

Dort auf "Aktivieren"
und bei **Optionen:**, unter **Ausführungsrichtlinie** klicken.
Die Auswahl **Lokale Skripts und remote signierte Skripts zulassen** dürfte RemoteSigned nahe kommen. (Nicht getestet.)



Repository vertrauen

	> Find-Module -Name "XpandPwsh"
	
	Version              Name                                Repository           Description
	-------              ----                                ----------           -----------
	1.211.0.5            XpandPwsh                           PSGallery            Various functions working with DevExpr...
	> Set-PSRepository -Name "XpandPwsh" -InstallationPolicy Trusted

Kontrolle

	> Get-PSRepository



	> Unregister-PSRepository -Name "PSGallery"





    Set-Location -Path C:\DevProjects

    clear-projectdirectories

    Clear-NugetCache


#		I N S T A L L A T I O N

Siehe https://dev.azure.com/MaxVallender1/_git/iXIS%20Doc
