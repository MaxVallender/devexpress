
 * **ASP** - Active Server Pages
 * **Blazor** - Web-Framework, unterteilt in Blazor Server, Blazor WebAssembly (clientseitig)
 * **BOModel** - Business Object Model
 * **dbo** (im SQL-Server) - database owner
 * **EF** - Entity Framework
 * **eXpandFramework** - Framework ?
 * **GAPTEQ** - Unternehmen, welches DevExpress im Unterbau hat ?
 * **MVC** - Model,View, Controller
 * **.NET** - .NET Framework
 * **.NET Core** - Modernes, verknaptes des .NET Frameworks
 * **ORM** - Object–Relational Mapping
 * **sa** (im SQL-Server) - system administrator
 * **WPF** - Windows Presentation Foundation
 * **XAF** - eXpressApp Framework
 * **xafml** - XAF/xml
 * **XAML**	- Extensible Application Markup Language
 * **WAsm** - siehe WebAssembly
 * **WebAssembly** - unter Blazor, Standart für portablen Binärcode
 * **Xpand** - siehe **eXpand**
 * **XPO**	- eXpress Persistent Object, ist ein ORM-Tool

